CREATE TABLE if not exists "users"(
    "id" SERIAL PRIMARY KEY NOT NULL,
    "first_name" VARCHAR(255) NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "password" VARCHAR NOT NULL,
    "phone_number" VARCHAR,
    "image_url" VARCHAR(255),
    "type" VARCHAR(255) CHECK ("type" IN('superadmin', 'customer')),
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL 
);

INSERT INTO users(first_name, last_name, email, password, type)
VALUES('Muhammadyusuf', 'Adhamov', 'muhammadyusufadhamov0+superadmin@gmail.com', '$2a$10$JT0HAAksN7kvv6m0TXAvIejUzNOs19uRA7Ae8qIjn5lLa2hP1isNK', 'superadmin');
INSERT INTO users(first_name, last_name, email, password, type)
VALUES('Muhammadyusuf', 'Adhamov', 'muhammadyusufadhamov0+customer@gmail.com', '$2a$10$JT0HAAksN7kvv6m0TXAvIejUzNOs19uRA7Ae8qIjn5lLa2hP1isNK', 'customer');