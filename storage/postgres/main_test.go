package postgres_test

import (
	"fmt"
	"log"
	"os"
	"testing"

_ "github.com/lib/pq"
	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/user_service/config"
	"gitlab.com/book_market/user_service/storage"
)

var strg storage.StorageI

func TestMain(m *testing.M) {
	cfg := config.Load("./../..")

	connStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)
	
	fmt.Println("------------", connStr)
	db, err := sqlx.Open("postgres", connStr)
	fmt.Println("------------", db)
	if err != nil {
		log.Fatalf("failed to open connection: %v", err)
	}

	strg = storage.NewStoragePg(db)
	os.Exit(m.Run())
}
