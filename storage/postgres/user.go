package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/user_service/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

func NewUser(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		db: db,
	}
}

func (u *userRepo) Create(user *repo.User) (*repo.User, error) {
	query := `
		insert into users(
			first_name,
			last_name,
			email,
			password,
			phone_number,
			image_url,
			type
		) values ($1,$2,$3,$4,$5,$6,$7)
		returning id, created_at
	`

	row := u.db.QueryRow(
		query,
		user.FirstName,
		user.LastName,
		user.Email,
		user.Password,
		user.PhoneNumber,
		user.ImageUrl,
		user.Type,
	)
	err := row.Scan(
		&user.ID,
		&user.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u *userRepo) Get(id int64) (*repo.User, error) {
	var result repo.User

	query := `
		select 
			id,
			first_name,
			last_name,
			email,
			password,
			phone_number,
			image_url,
			type,
			created_at
		from users 
		where id = $1
	`

	row := u.db.QueryRow(query, id)

	err := row.Scan(
		&result.ID,
		&result.FirstName,
		&result.LastName,
		&result.Email,
		&result.Password,
		&result.PhoneNumber,
		&result.ImageUrl,
		&result.Type,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (u *userRepo) GetAll(params *repo.GetAllUsersParams) (*repo.GetAllUsersResult, error) {
	result := repo.GetAllUsersResult{
		Users: make([]*repo.User, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" limit %d offset %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter += fmt.Sprintf(`
			where first_name ilike '%s' or last_name ilike '%s' or email ilike '%s' or phone_number ilike '%s'`,
			str, str, str, str,
		)
	}

	query := `
		select 
			id,
			first_name,
			last_name,
			email,
			password,
			phone_number,
			image_url,
			type,
			created_at
		from users
	` + filter + `
		order by created_at desc
	` + limit

	rows, err := u.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var u repo.User
		err := rows.Scan(
			&u.ID,
			&u.FirstName,
			&u.LastName,
			&u.Email,
			&u.Password,
			&u.PhoneNumber,
			&u.ImageUrl,
			&u.Type,
			&u.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Users = append(result.Users, &u)
	}

	queryCount := `select count(1) from users ` + filter
	err =u.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (u *userRepo) GetByEmail(email string) (*repo.User, error) {
	var result repo.User
	var phoneNumber, imageUrl sql.NullString

	query := `
		select 
			id,
			first_name,
			last_name,
			email,
			password,
			phone_number,
			type,
			image_url,
			created_at
		from users
		where email=$1
	`

	row := u.db.QueryRow(query, email)
	err := row.Scan(
		&result.ID,
		&result.FirstName,
		&result.LastName,
		&result.Email,
		&result.Password,
		&phoneNumber,
		&result.Type,
		&imageUrl,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}
	result.PhoneNumber = phoneNumber.String
	result.ImageUrl = imageUrl.String

	return &result, nil
}

func (u *userRepo) UpdatePassword(req *repo.UpdatePassword) error {
	query := `update users set password=$1 where id=$2`

	_, err := u.db.Exec(query, req.Password, req.UserID)
	if err != nil {
		return err
	}

	return nil
}

func (u *userRepo) Update(user *repo.User) (*repo.User, error) {
	query := `
		update users set
			first_name = $1,
			last_name = $2,
			phone_number = $3,
			image_url = $4
		where id = $5
		returning email, type, created_at
	`

	err := u.db.QueryRow(
		query,
		user.FirstName,
		user.LastName,
		user.PhoneNumber,
		user.ImageUrl,
		user.ID,
	).Scan(
		&user.Email,
		&user.Type,
		&user.CreatedAt,
	)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u *userRepo) Delete(id int64) error {
	query := `delete from users where id=$1`

	result, err := u.db.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil 
}