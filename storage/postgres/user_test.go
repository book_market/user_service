package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/book_market/user_service/storage/repo"
)

func CreateUser(t *testing.T) *repo.User {
	u, err := strg.User().Create(&repo.User{
		FirstName: faker.FirstName(),
		LastName:  faker.LastName(),
		Email:     faker.Email(),
		Password:  faker.Password(),
		Type:      repo.UserTypeCustomer,
	})
	require.NoError(t, err)
	require.NotEmpty(t, u)

	return u
}

func TestCreateUser(t *testing.T) {
	CreateUser(t)
}

func TestGetUser(t *testing.T) {
	c := CreateUser(t)

	user, err := strg.User().Get(c.ID)
	require.NoError(t, err)
	require.NotEmpty(t, user)	
}

func TestDeleteUsers(t *testing.T) {
	c := CreateUser(t)
	err := strg.User().Delete(c.ID)
	require.NoError(t, err)
}

func TestGetAllUsers(t *testing.T) {
	user, err := strg.User().GetAll(&repo.GetAllUsersParams{
		Limit: 3,
		Page: 1,
		Search: "ab",
	})
	require.NoError(t, err)
	require.NotEmpty(t, user)
}

func TestUpdateUser(t *testing.T) {
	c := CreateUser(t)

	user, err := strg.User().Update(&repo.User{
		ID: c.ID,
		FirstName: faker.FirstName(),
		LastName:  faker.LastName(),
		Email:     faker.Email(),
		Password:  faker.Password(),
		Type:      repo.UserTypeCustomer,
	})
	require.NoError(t, err)
	require.NotEmpty(t, user)
}

func TestGetByEmail(t *testing.T) {
	c := CreateUser(t)

	user, err := strg.User().GetByEmail(c.Email)
	require.NoError(t, err)
	require.NotEmpty(t, user)
}

