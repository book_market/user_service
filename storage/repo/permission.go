package repo

type PermissionStorageI interface {
	CheckPermission(userType, reaource, action string) (bool, error)
}